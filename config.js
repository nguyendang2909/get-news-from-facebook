export default {
  userFB: 'quynhanh290991@gmail.com',
  passFB: 'onlyone2',
  port: 1234,
  options: {
    title: process.env.npm_package_name,
    version: process.env.npm_package_version,
    description: 'Get news from Facebook',
    termsOfService: 'reason to use the service',
    contact: {
      name: 'LD3',
      url: '',
      email: 'ld3@email.com',
    },
    license: {
      name: 'LD3',
      url: 'https://LD3.com',
    },
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
      {
        name: 'Get news',
        description: 'Get news from facebook',
      },
      {
        name: 'Recipes',
        description: 'Everything about your receipes',
      },
    ],
  },
  cronjob: [
    'viettan',
  ],
};
