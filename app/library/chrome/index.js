import puppeteer from 'puppeteer';
import config from '../../../config';
import logger from '../logger';

const ID = {
  login: '#email',
  pass: '#pass',
};

class Chrome {
  async init() {
    const browser = await puppeteer.launch({
      // Hide Chrome or not?
      headless: false,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-notifications',
      ],
      defaultViewport: {
        width: 1100,
        height: 600,
      },
    });
    this.page = await browser.newPage();
    await this.page.goto('https://facebook.com', {
      waitUntil: 'networkidle2',
    });
    await this.page.waitForSelector(ID.login);
    await this.page.type(ID.login, config.userFB);
    await this.page.type(ID.pass, config.passFB);
    await this.page.click('#loginbutton');
    logger.info('Chrome initialized!');
  }
}

export default new Chrome();
