import logger from './library/logger';
import API from './api';
import Chrome from './library/chrome';
import cronjob from './cronjob';

(async () => {
  try {
    await Chrome.init();
    await API();
    logger.info('Application initialized!');
    cronjob();
  } catch (error) {
    logger.error({ message: 'Error while initializing application', error });
  }
})();
