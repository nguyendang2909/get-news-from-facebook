import express from 'express';
import asyncHandler from 'express-async-handler';
import swagger from 'swagger-spec-express';
import getNewsFromPageFB from './handler/getNewsFromPageFB';

const router = express.Router();
swagger.swaggerize(router);
router.get('/a', (req, res) => {
  console.log('receive request');
  res.send('1111')
})
router.get('/news-page/:pageId', asyncHandler(getNewsFromPageFB));

export default router;
