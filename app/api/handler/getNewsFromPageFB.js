import Chrome from '../../library/chrome';

// Chrome.init();

export default async function getNewsFromPageFB(req, res) {
  let pageId;
  if (req.params) { pageId = req.params.pageId; }

  await Chrome.page.goto(`https://vi-vn.facebook.com/${pageId}/posts`, {
    waitUntil: 'networkidle2',
  });

  // Expand see more
  const seeMoreLinks = await Chrome.page.$$('a.see_more_link');

  for (let i = 0; i < seeMoreLinks.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await seeMoreLinks[i].click({ delay: 100 });
  }

  const results = await Chrome.page.evaluate(() => {
    const items = document.querySelectorAll('.text_exposed_root');
    const links = [];
    items.forEach((item) => {
      links.push({
        title: item.innerText,
        // url: item.getAttribute('class'),
      });
    });
    return links;
  });
  res.send(results);
  // await browser.close();
}
